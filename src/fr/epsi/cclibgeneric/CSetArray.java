package fr.epsi.cclibgeneric;

import fr.epsi.cclibgeneric.exception.CSetEmptyException;
import fr.epsi.cclibgeneric.exception.CSetGenericException;
import fr.epsi.cclibgeneric.exception.CSetIndexOutOfBoundException;

import java.lang.reflect.Array;

public class CSetArray<T> implements ICSet<T> {
	
	public static final int DEFAULT_MAX_SIZE = 10;
	
	private T[] data;
	private int currentIndex;
	
	{
		currentIndex = -1;
	}
	
	public CSetArray(Class<T> type) {
		data = ( T[] ) Array.newInstance( type, DEFAULT_MAX_SIZE );
	}
	
	public CSetArray(Class<T> type, int maxSize ) {
		data = ( T[] ) Array.newInstance( type, maxSize );
	}
	
	public CSetArray( T[] data ) {
		this.data = data;
	}
	
	@Override
	public void add( T object ) throws CSetGenericException {
	
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public T get( int index ) throws CSetIndexOutOfBoundException {
		return null;
	}
	
	@Override
	public boolean contains( T object ) {
		return false;
	}
	
	@Override
	public int getSize() {
		return 0;
	}
	
	@Override
	public int getMaxSize() {
		return 0;
	}
	
	@Override
	public ICSet<T> copy() {
		return null;
	}
	
	@Override
	public Object remove( int index ) throws CSetEmptyException, CSetIndexOutOfBoundException {
		return null;
	}
	
	@Override
	public void merge( ICSet<T> set ) {
	
	}
}
