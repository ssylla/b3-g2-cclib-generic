package fr.epsi.cclibgeneric.exception;

public class CSetEmptyException extends Exception {
	
	public CSetEmptyException( String message ) {
		super( message );
	}
}
