package fr.epsi.cclibgeneric.exception;

public class CSetIndexOutOfBoundException extends Exception {

	private int index;
	private int size;
	
	public CSetIndexOutOfBoundException( int index, int size ) {
		this.index = index;
		this.size = size;
	}
	
	@Override
	public String getMessage() {
		return "Attention, la taille max est : "+size+" (index passé en param : "+index+")";
	}
	
	public int getIndex() {
		return index;
	}
	
	public int getSize() {
		return size;
	}
}
