package fr.epsi.cclibgeneric;

import fr.epsi.cclibgeneric.exception.CSetEmptyException;
import fr.epsi.cclibgeneric.exception.CSetGenericException;
import fr.epsi.cclibgeneric.exception.CSetIndexOutOfBoundException;

public interface ICSet<T> {
	
	void add( T object ) throws CSetGenericException;
	
	boolean isEmpty();
	
	T get( int index ) throws CSetIndexOutOfBoundException;
	
	boolean contains( T object );
	
	int getSize();
	
	int getMaxSize();
	
	ICSet<T> copy();
	
	Object remove( int index ) throws CSetEmptyException, CSetIndexOutOfBoundException;
	
	void merge( ICSet<T> set );
}
